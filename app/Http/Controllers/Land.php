<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Land extends Controller
{
    public function index()
    {
        return view("land",["title"=>"My First Laravel Land"]);
    }

    public function success()
    {
        return view("success");
    }
}
